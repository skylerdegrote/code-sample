Code Example - HTML, JavaScript, Bootstrap, CSS, AngularJS. I'm an avid learner, and I have taught myself the basics and beyond in multiple front end, client side, server side and back end languages. Please reach out with any questions.


//The original code challenge instructions
Note: I was given some base code that I had to sift through and work with.
Modify the Election App in the following ways (note firefox often works better than other browsers for local file development)

1. Add a column to the results table to calculate the percent of total votes recieved (should be to the right of vote counts)
    • done
2. Dynamically reorder the list of candidates based on number of votes recieved (the candidate with the most votes should be at the top of the list)
    • done
3. Allow the user to add a new candidate so that the candidate will show up in the results and user can cast votes (use the existing stub  Election.Candidate.Component).
    • done
4. In the manage candidates component list the current candidates and give the user the ability to delete a candidate from the race.
    • done
5. Implement Styling to make the application look more professional and user friendly
    • done