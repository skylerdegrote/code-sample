(function ($scope) {
    "use strict";

    angular.module("Election.App", [
        "Election.Component"
    ]);

})();




//Election Component
(function () {
    "use strict";

    angular.module("Election.Component", [
        "Election.Candidate.Component",
        "Election.Results.Component",
        "Election.Vote.Component"
    ])
        .component("tfElection", {
            templateUrl: "App/Election.Component.Template.html",
            controller: ElectionController,
            bindings: { }
        });

    ElectionController.$inject = [ "$timeout" ];

    function ElectionController($timeout){
        var ctrl = this;

        ctrl.candidates = [];

        ctrl.onCandidateCreate = function(candidate) {
            ctrl.candidates.push(candidate);
        };

        ctrl.onCandidateDelete = function(candidate) {
            var index = ctrl.candidates.indexOf(candidate);
            ctrl.candidates.splice(index, 1);
        };

        ctrl.onVote = function(candidate) {
            var index = ctrl.candidates.indexOf(candidate);
            ctrl.candidates[index].votes += 1;
        };


        ctrl.$onInit = function() {


            var calculateTotal = function() {
                return _.sumBy(ctrl.candidates, function(candidates){
                    return candidates.votes;
                });
            };
            ctrl.total = calculateTotal() ;










            // Example Initial Data Request
            // Mimic 1 seconds ajax call
            $timeout(function(){
                ctrl.candidates = [
                    { name: "Puppies", color: "blue", votes: 1},
                    { name: "Kittens", color: "red", votes: 1},
                    { name: "Pandas", color: "green", votes: 1}
                ];
/////calculate the total votes between all candidates
                var calculateTotal = function() {
                    return _.sumBy(ctrl.candidates, function(candidates){
                        return candidates.votes;
                    });
                };
                ctrl.total = calculateTotal() ;
////calculate the percentage of total votes for each of the objects within the candidates array
                for(var i=0;i<ctrl.candidates.length;i++){
                    ctrl.candidates[i].percent=Number((ctrl.candidates[i].votes*100/(calculateTotal())).toFixed(2));
                }

            }, 1000);

        };
    }

})();

//Candidate Component
(function (angular, $scope) {
    "use strict";

    angular.module("Election.Candidate.Component", [])
        .component("tfElectionCandidate", {
            templateUrl: "App/Election.Candidate.Component.Template.html",
            controller: CandidateController,
            bindings: {
                onCreate: "&",
                onDelete: "&",
                candidates: "<"
            }
        });
    function CandidateController(){
        var ctrl = this,
            buildNewCandidate = function() {
                return {
                    votes: 1,
                    name: ctrl.name,
                    color: null
                };
            };

        ctrl.newCandidate = null;
        CandidateController.$inject = [];







        //TODO Add code to add a new candidate
        ctrl.addCandidate = function() {
            ctrl.candidates.push({ name: ctrl.name, votes:1 });
            /////calculate the total votes between all candidates
            var calculateTotal = function() {
                return _.sumBy(ctrl.candidates, function(candidates){
                    return candidates.votes;
                });
            };
            ctrl.total = calculateTotal() ;
//calculate the percentage of total votes for each of the objects within the candidates array and change on click of
// vote buttons
            for(var i=0;i<ctrl.candidates.length;i++){
                ctrl.candidates[i].percent=Number((ctrl.candidates[i].votes*100/(calculateTotal())).toFixed(2));
            }
        };
        //TODO Add code to remove a candidate




//delete
        ctrl.delete = function($index){
            ctrl.candidates.splice($index, 1);
            /////calculate the total votes between all candidates
            var calculateTotal = function() {
                return _.sumBy(ctrl.candidates, function(candidates){
                    return candidates.votes;
                });
            };
            ctrl.total = calculateTotal() ;
//calculate the percentage of total votes for each of the objects within the candidates array and change on click of
// vote buttons
            for(var i=0;i<ctrl.candidates.length;i++){
                ctrl.candidates[i].percent=Number((ctrl.candidates[i].votes*100/(calculateTotal())).toFixed(2));
            }
        };
        // $onInit is called once at component initialization
        ctrl.$onInit = function () {
            ctrl.newCandidate = buildNewCandidate();
        };
    }

})(window.angular);

//Result Component
(function () {
    "use strict";

    angular.module("Election.Results.Component", [])
        .component("tfElectionResults", {
            templateUrl: "App/Election.Results.Component.Template.html",
            controller: ResultsController,
            bindings: {
                candidates: "<"
            }
        });

    ResultsController.$inject = [];

    function ResultsController(){
        var ctrl = this;

        ctrl.getCandidatePercentage = function (votes) {
            var total = _.sumBy(ctrl.candidates, "votes");
            if (total) {
                return 100 * votes / total;
            }
            return 0;
        };
    }

})();

//Vote Component
(function () {
    "use strict";

    angular.module("Election.Vote.Component", [])
        .component("tfElectionVote", {
            templateUrl: "App/Election.Vote.Component.Template.html",
            controller: VoteController,
            bindings: {
                candidates: "<",
                onVote: "&"
            }
        });

    VoteController.$inject = [];

    function VoteController(){
        var ctrl = this;

        ctrl.castVote = function (candidate) {
            ctrl.onVote({ $candidate: candidate });
            var calculateTotal = function() {
                return _.sumBy(ctrl.candidates, function(candidates){
                    return candidates.votes;
                });
            };
            ctrl.total = calculateTotal() ;
//calculate the percentage of total votes for each of the objects within the candidates array and change on click of
// vote buttons
            for(var i=0;i<ctrl.candidates.length;i++){
                ctrl.candidates[i].percent=Number((ctrl.candidates[i].votes*100/(calculateTotal())).toFixed(2));
            }




        };
    }

})();